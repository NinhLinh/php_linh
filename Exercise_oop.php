<?php

use Affiliate as GlobalAffiliate;
use StoreOwner as GlobalStoreOwner;

class Customer {
    public $nameCustomer;
    public $address;
    public Affiliate $affiliate;

    public function __construct($name, $address) {
        $this->name = $name;
        $this->address = $address;
    }
    
    /**
     * mua hàng, trong đó 1 phần giá trị đơn hàng được chuyển vào Balance của các Affiliate, phần còn lại chuyển vào 
     * Balance của Store Owner
     *
     * @param [type] $total
     * @param StoreOwner $storeOwner
     * @return void
     * 
     */
    public function placeOrder($total, $storeOwner) {
        if ($this->affiliate) {
            $commission =  $total * 0.1;
            if ($this->affiliate->upperAffiliate){
                $uppcommision = $total * 0.05;
                $this->affiliate->upperAffiliate->balance += $uppcommision;
                $total -= $uppcommision;
            }
            $this->affiliate->balance += $commission;
            $total -= $commission;
        }
        $storeOwner->balance += $total;
    }
}

class Order {
    public $total;

    public function __construct($total) {
        $this->total = $total;
    }
}

class Affiliate {
    public $name;
    public $balance;
    public $upperAffiliate;
    public $subAffiliates;
    public $customers;
  
    public function __construct($name, $balance, $upperAffiliate) {
        $this->name = $name;
        $this->balance = $balance;
        $this->upperAffiliate = $upperAffiliate;
        $this->subAffiliates = [];
        $this->customers = [];
    }

    /**
     * giới thiệu người khác (Customer hoặc Affiliate) đến của hàng
     *
     * @param Affiliate|Customer $person
     * @return void
     */
    public function refer($person) {
        if ($person instanceof Customer) {
            $this->customers[] = $person;
            $person->affiliate = $this;
        } else if ($person instanceof Affiliate) {
            $this->subAffiliates[] = $person;
            $person->upperAffiliate = $this;
        }
    }

    /**
     * rút tiền từ Balance, không thực hiện được nếu Balance < $100. Hiện thông báo sau khi thực hiện withdraw.
     *
     * @return void
     */
    public function withdraw($amount) {
        if ($amount >= 100) {
            if ($this->balance >= $amount) {
                $this->balance -= $amount;
                echo "Withdraw " . $amount . " from balance of Affiliate " . $this->name . "\n";
                echo "Số dư hiện tại là: " . $this->balance;
                //$amount = 0;
            } else {
                echo "Cannot withdraw from balance of Affiliate " . $this->name . "\n";
            }
        } else {
            echo "Cannot withdraw from balance of Affiliate " . $this->name . "\n";
        }
    }

    /**
     * PrintSubAff: in ra danh sách các Affiliate mà đối tượng này giới thiệu đến cửa hàng
     *
     * @return void
     */
    public function printSubAffiliates() {
        echo "List of sub Affiliates of Affiliate " . $this->name . "\n";
        foreach ($this->subAffiliates as $affiliate) {
            echo $affiliate->name . " ";
        }
        echo "\n";
    }

    /**
     * PrintCustomers: in ra danh sách các Customer mà đối tượng này giới thiệu đến cửa hàng
     *
     * @return void
     */
    public function printCustomers() {
        echo "List of customers of Affiliate: " . $this->name . "\n";
        foreach ($this->customers as $customer) {
            echo $customer->name . " ";
        }
        echo "\n";
    }
}
  
class StoreOwner {
    public $name;
    public $balance;
  
    public function __construct($name, $balance) {
      $this->name = $name;
      $this->balance = $balance;
    }
  
    public function printBalance() {
        echo "Balance of Store Owner is: " . $this->name . ": " . $this->balance . "\n";
    }
}

//Tạo đối tượng chủ cửa hàng ($name, $balance)
$moyes = new StoreOwner("moyes", 100000);

/* Tạo đối tượng Affiliate John giới thiệu các đối tượng Affiliate Sarah, Eva, Jimmy, Henry.
   ($name, $balance, $upperAffiliate)
*/
$john = new Affiliate("John", 300, null);
$sarah = new Affiliate("Sarah", 100, $john);
$eva = new Affiliate("Eva", 50, $john);
$jimmy = new Affiliate("Jimmy", 150, $john);
$henry = new Affiliate("Henry", 20, $john);

//Tao doi tuong customer
$johnCus = new Customer("John Doe", "123 Tran Thai Tong");
$sarahCus = new Customer("Jane Doe", "45 Tay Ho");
$evaCus = new Customer("John Smith", "119 Trung Kinh");
$jimmyCus = new Customer("Jane Smith", "40 Ho Tay");
$henryCus = new Customer("John Jones", "436 Ngoc Lam");

// Mỗi affiliate này lại giới thiệu được 1 khách hàng. ($name, $address, $affiliate)
$john->refer($johnCus);
$sarah->refer($sarahCus);
$eva->refer($evaCus);
$jimmy->refer($jimmy);
$henry->refer($henryCus);

// Mỗi khách hàng mua 1 đơn hàng trị giá $800.
$johnCus->placeOrder(800, $moyes);
$sarahCus->placeOrder(800, $moyes);
$evaCus->placeOrder(800, $moyes);
$jimmyCus->placeOrder(800, $moyes);
$henryCus->placeOrder(800, $moyes);

//Rut tien
echo $john->withdraw(300);
echo "</br>";
echo $john->withdraw(150);
echo "</br>";
echo $sarah->withdraw(50);

//In ra tổng tiền thu được của chủ store
echo"</br>";
echo $moyes->printBalance();

echo"</br>";
echo $john->printSubAffiliates();
